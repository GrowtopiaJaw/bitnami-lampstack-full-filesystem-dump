TARGETS = console-setup resolvconf mountkernfs.sh ufw screen-cleanup hostname.sh x11-common plymouth-log apparmor udev cryptdisks cryptdisks-early hwclock.sh mountdevsubfs.sh checkroot.sh urandom open-iscsi networking iscsid lvm2 checkfs.sh kmod mountall.sh bootmisc.sh checkroot-bootclean.sh mountnfs-bootclean.sh mountnfs.sh mountall-bootclean.sh procps
INTERACTIVE = console-setup udev cryptdisks cryptdisks-early checkroot.sh checkfs.sh
udev: mountkernfs.sh
cryptdisks: checkroot.sh cryptdisks-early udev lvm2
cryptdisks-early: checkroot.sh udev
hwclock.sh: mountdevsubfs.sh
mountdevsubfs.sh: mountkernfs.sh udev
checkroot.sh: hwclock.sh mountdevsubfs.sh hostname.sh
urandom: hwclock.sh
open-iscsi: networking iscsid
networking: resolvconf mountkernfs.sh urandom procps
iscsid: networking
lvm2: cryptdisks-early mountdevsubfs.sh udev
checkfs.sh: cryptdisks lvm2 checkroot.sh
kmod: checkroot.sh
mountall.sh: lvm2 checkfs.sh checkroot-bootclean.sh
bootmisc.sh: udev checkroot-bootclean.sh mountnfs-bootclean.sh mountall-bootclean.sh
checkroot-bootclean.sh: checkroot.sh
mountnfs-bootclean.sh: mountnfs.sh
mountnfs.sh: networking
mountall-bootclean.sh: mountall.sh
procps: mountkernfs.sh udev
